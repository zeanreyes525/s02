GIT VCS

Whhat is GIT VCS?
- Git BCS is an open source version control system. 
- It allows us to track and update our files with only the changes made to it.

GitLab vs GitHub
	There is not much difference between the two. Both Gitlab and Github are just competing cloud services that store and manage our online respiratory.

Two Types of Repositories
- Local Repositories
	- Folders that use git technology. Therefore, it allows us to track changes made in the files with the folder. These changes can be then uploaded and updated the files in our Remote Repositories.
- Remote Repositories 
	- These are the folders that use git technology aswell but isntead located in the internet or in cloud services such as GitLab and GitHub.

What is an SSH Key?
- SSH or Secure Shell Key are tools we use to authenticate the uploading or of other tasks when manipulating or using git repositories.
- Allows us to push/upload changes to our repos without the need of password.

Git Commands:
	- git status
		- to peek at the status of the files/folders in the repository
	- git init
		- to initialize a local Git repository

Git config
	These commands will allow us to have git recognize the person trying to push into an online/remote repo:

git config --global user.email "email used in gitlab"
	- configure the email used to push into the remote repo.

git config --global user.name "user name used in gitlab"
	- configure the name/username of the user trying to push  into the gitLab

git  commit -m "<commitMessage>"
	- to create a new commit or version of our files to be pused into our remote repo.

git log
	- to check the version history of the repistory

git remote add <aliasOfRemote> <urlOfRemote>
	- to add/connect a remote repository to our local repository

git remote -v
	- to view the remote repositories connected to our local repo

git push <alias> master
	- to push our updates/changes/version/commit into our remote repository

/*Mini-Activity*/
1. Create a file named myself.html inside the activity folder
2. Add Myself inside the title tag
3. Save changes and apply git commands
	- git status
	- git init to initialize
	- git add . and git commit
		- git commit message "Initial commit"
4. Add a line that contains your full name, age, course, and hobbies
5. Save changes and do git commands to save and track changes
	- git add and git commit
		- git commit message "Update the personal information"
6. Add an empty line after the above information that describes your mmotivation in joining your course.
7. Save changes and do git commands to save and  track changes
	- git add . and git commit
		- git commit message "Include motivation in joining the course"
8. View the version history
9. Upload your file in your remote repository.

Post your URL in our SB discussion